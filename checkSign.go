package main

import (
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"
)

func checkSign(cookie []*http.Cookie) signStatus {
	status := signStatus{false, false, 0, ""}
	var errOut any
	url := `https://www.t00ls.cc/members-tubilog-12513.html`
	resultRequest := myRequest{url: url, method: "GET", cookie: cookie, ssl: true}
	resultResp, err := resultRequest.send()
	if err != nil {
		errOut = err.Error()
		panic(errOut)
	} else {
		resultContext, _ := ioutil.ReadAll(resultResp.Body)
		resultResp.Body.Close()
		times := time.Now().Add(time.Hour * 8) //东八区
		//currentData := fmt.Sprintf("%v-%v-%v", times.Year(), times.Month(), times.Day())
		currentData := strings.Split(times.String(), " ")[0]
		status.Date = currentData
		redate := regexp.MustCompile("<td class=\"data\">\\s*([^<]+?)\\s*</td>")
		rs := redate.FindAllStringSubmatch(string(resultContext), -1)
		for {
			if len(rs) >= 4 {
				core, err := strconv.Atoi(rs[2][1])
				if err != nil {
					errOut = err.Error()
					panic(errOut)
				} else {
					if core > status.Core {
						status.Core = core
					}
				}
				if strings.Contains(rs[1][1], currentData) && rs[3][1] == "网站签到" {
					status.IsSign = true
				}
				if strings.Contains(rs[1][1], currentData) && strings.Contains(rs[3][1], "域名") {
					status.IsQueryDomain = true
				}
				rs = rs[4:]
			} else {
				break
			}
		}
	}
	return status
}
