package main

import (
	"io/ioutil"
	"log"
	"regexp"
)

type queryDomain struct {
	url    string
	method string
	header map[string]string
	data   any
	ssl    bool
}

func (req *queryDomain) query() (domains []string) {
	request := myRequest{url: req.url, header: &req.header, method: req.method, ssl: req.ssl}
	resp, err := request.send()
	if err != nil {
		log.Println(err.Error())
	} else {
		defer resp.Body.Close()
		context, _ := ioutil.ReadAll(resp.Body)
		reg := regexp.MustCompile(`>([\w-.]+\.com)</a>`)
		rs := reg.FindAllStringSubmatch(string(context), -1)
		if len(rs) != 0 {
			for _, v := range rs {
				domains = append(domains, v[1])
			}
		}
	}
	return
}

func getDomain(site string) (domains []string) {
	header := make(map[string]string)
	header["Content-Type"] = "application/x-www-form-urlencoded"
	header["User-Agent"] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Safari/537.36"

	yooymDomain := queryDomain{
		url:    "https://www.yooym.com/mi.php?Page=houz=2&paixu=1",
		method: "GET",
		header: header,
		ssl:    true,
	}

	chinazDomain := queryDomain{
		url:    "https://whois.chinaz.com/suffix",
		method: "POST",
		data:   "suffix=.com&c_suffix=&time=1&startDay=&endDay=",
		ssl:    true,
		header: header,
	}

	ip138Domain := queryDomain{
		url:    "https://site.ip138.com/",
		ssl:    true,
		header: header,
		method: "GET",
	}

	switch site {
	case "chinaz":
		domains = chinazDomain.query()
	case "ip138":
		domains = ip138Domain.query()
	case "yooym":
		domains = yooymDomain.query()
	}
	return
}
